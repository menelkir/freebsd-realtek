Realtek Ethernet Drivers for FreeBSD
====================================

=============
What is this?
=============

It's a driver for Realtek Ethernet cards in case the one in kernel
doesn't work for you.
This driver is modified by Realtek Semiconductor corp. and it has been tested OK
on FreeBSD v5.4 v6.4, v7.3, v8.0 and v9.0 (tested by original author: fkoczan), 
FreeBSD v11.0, v12.0 and v13.0-CURRENT (tested by me: Menelkir).
To update the driver, you may use method 1. If method 1 failed, you must use 
method 2 which is more complex.


=========================
What cards are supported?
=========================

Tested so far:

| 8169S/8169SB/8169SC/8168B/8168C/8168CP/8168D/8168DP/8168E/8168F      
| 8168FB/8168G/8168GU/8168H/8168EP/8411/RTL8168FP/8101E/8102E/
| 8103E/8111/8168/8411/8401/8105E/8106E/8107E/8402 


======================================
What FreeBSD version this should work?
======================================

**Tested by fkoczan:**

* v5.4
* v6.4
* v7.3
* v8.0
* v9.0 

**Tested by me (Menelkir):**

* v11.x
* v12.x
* v13.x-CURRENT

============
Requirements
============

1. Make sure you have the source code in /usr/src
2. if there's a kernel update, you'll also need to reinstall this driver

=====
Howto
=====

**Method 1:**

1. Clone this repository

	| `git clone https://gitlab.com/menelkir/freebsd-realtek`

2. Build

	| `make`
    
3. Copy the resulting if_re.ko into /boot/kernel, overwriting the one from the kernel

	| `cp if_re.ko /boot/kernel`
    
4. Make sure you have if_re_load="YES" in your /boot/loader.conf 
5. Make sure your network is well configured
6. Reboot

**Method 2:**

Because the FreeBSD kernel has default drivers to support RTL8139C and RTL8169S. 
To use the RTL8139C+, RTL8169SB, RTL8169SC, RTL8168B, and RTL8101E, 
you need to update your NIC driver by recompiling your FreeBSD kernel.
|The main steps you have to do:(FreeBSD SrcDir means the directory of 
FreeBSD source code and it may be "/usr/src/sys"):

1. Keep the original driver source code:

	| `cd /usr/src/sys/dev/re`
	| `cp if_re.c if_re.c.org`
	| `cd /usr/src/sys/modules`
	| `cp Makefile Makefile.org`
	| `cd /usr/src/sys/modules/re`
	| `cp Makefile Makefile.org`
	| `cd /usr/src/sys/i386/conf`
	| `cp GENERIC GENERIC.org`

2. Recompile your kernel (you must install your FreeBSD source code first!):

2.1. Delete "re" entry:

	| `vi /usr/src/sys/i386/conf/GENERIC`
	| `vi /usr/src/sys/modules/Makefile`

2.2. Configure and compile the kernel:

	| `cd /usr/src/sys/i386/conf`
	| `/usr/sbin/config GENERIC`
	| `cd ../compile/GENERIC`
	| `make cleandepend`
	| `make depend`
	| `make`
	| `make install`
	| `reboot`

3. Update the driver source code:

	| `cp if_re.c /usr/src/sys/dev/re`
	| `cp if_rereg.h /usr/src/sys/dev/re`
	| `cp Makefile /usr/src/sys/modules/re`

4. Build the driver:

	| `cd /usr/src/sys/modules/re`
	| `make clean`
	| `make`

5. Install the driver

	| `cd /usr/src/sys/modules/re`
	| `kldload ./if_re.ko`

6. Configure the static IP address

	| `ifconfig re0 xxx.xxx.xxx.xxx`

7. Configure the IP address by DHCP

	| `/sbin/dhclient re0`

8. Test. If everything is fine, proceed to regular network configuration.

=====================
Link Speed and Duplex
=====================

The user can use the following command to change link speed and duplexmode:

1. For auto negotiation:

	| `ìfconfig re<device_num> media autoselect`

2. For 1000Mbps full-duplex:

	| `ifconfig re<device_num> media 1000baseTX mediaopt full-duplex`

3. For 100Mbps full-duplex:

	| `ifconfig re<device_num> media 100baseTX mediaopt full-duplex`

4. For 100Mbps half-duplex:

	| `ifconfig re<device_num> media 100baseTX -mediaopt full-duplex`

5. For 10Mbps full-duplex:

	| `ifconfig re<device_num> media 10baseT/UTP mediaopt full-duplex`

6. For 10Mbps half-duplex:

	| `ifconfig re<device_num> media 10baseT/UTP -mediaopt full-duplex`

=======
Credits
=======

| https://gist.github.com/jovimon/524e116471f249626fd2ccd141f3fe05
| https://forum.netgate.com/topic/133536/official-realtek-driver-v1-95-binary/21
| https://forums.freebsd.org/threads/replacing-realtek-re-driver.55861

=============
How to donate
=============

If you find this repo useful, send a coffee to the author:

:fkoczan: https://paypal.me/fkoczan

or a beer to me:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c
